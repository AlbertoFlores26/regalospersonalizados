@extends('layouts.nav')

@section('content')


<div class="container">
  @foreach ($productos as $orden=>$producto)
    <div class="row mt-5">
      <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
        <a href="{{ route('Tienda.create', $producto->id ) }}">
          <img src="{{ asset('imagen-producto/'. $producto->foto ) }}" class="card-img-top" alt="...">
          <h4 class="text-center" style="color: #F89CA4; font-family: 'Rancho', cursive;">{{$producto->descripcionProducto}}</h4>
          <h5 class="text-center" style="font-family: 'Rancho', cursive;">${{$producto->precio}}</h5>
        </a>
      </div>
    </div>
  @endforeach
  
  <nav aria-label="Page navigation example" class="pagination justify-content-center">
    <ul class="pagination">
      <li class="page-item">
        <a class="" href="#" aria-label="Previous">
          
        </a>
      </li>
      <li class="page-item"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item">
        <a class="" href="#" aria-label="Next">
          
        </a>
      </li>
    </ul>
  </nav>

</div>




@endsection