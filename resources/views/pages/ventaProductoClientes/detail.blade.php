@extends('layouts.nav')

    @section('content')
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div> <!--Buscador-->

        <div class="container-fluid mt--7" style="">
            <div class="card bg-white shadow" style="padding-bottom: 20px">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Información Regalo</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('Compras') }}" class="btn btn-sm btn-primary">Regresar</a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row mt-5">
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                        <img src="{{ asset('imagen-producto/'. $venta->productos->foto ) }}" class="card-img-top" alt="...">
                            <h4 class="text-center" style="color: #F89CA4; font-family: 'Rancho', cursive;"></h4>
                            <h5 class="text-center" style="font-family: 'Rancho', cursive;">$</h5>
                        </div>
                    </div>
                    <h3 class="text-center">Información</h3>
                    <div class="row form-group">
                    <div class="col-12">
                        <label for="">Frase</label>
                            <input type="text"  name="frase_producto" class="form-control" maxlength="20" value="{{$venta->frase_producto}}" readonly>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                        <label for="">Mza</label>
                            <input type="text" id="mza" name="mza" class="form-control" value="{{$venta->direccion_ventas->mza}}" readonly>
                        </div>
                        <div class="col-6">
                            <label for="">lote</label>
                            <input type="text" id="lote" name="lote" class="form-control" value="{{$venta->direccion_ventas->lote}}" readonly>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                        <label for="">calle</label>
                            <input type="text" id="calle" name="calle" class="form-control" value="{{$venta->direccion_ventas->calle}}" readonly>
                        </div>
                        <div class="col-6">
                            <label for="">colonia</label>
                            <input type="text" id="colonia" name="colonia" class="form-control" value="{{$venta->direccion_ventas->colonia}}" readonly>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                        <label for="">noExterior</label>
                            <input type="text" id="noExterior" name="noExterior" class="form-control" value="{{$venta->direccion_ventas->noExterior}}" readonly>
                        </div>
                        <div class="col-6">
                            <label for="">noInterior</label>
                            <input type="text" id="noInterior" name="noInterior" class="form-control" value="{{$venta->direccion_ventas->noInterior}}" readonly>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label for="">Smza</label>
                            <input type="text" id="smza" name="smza" class="form-control" value="{{$venta->direccion_ventas->smza->smza}}" readonly>
                        </div>
                        <div class="col-6">
                            <label for="">cpp</label>
                            <input type="text" id="cpp" name="cpp" class="form-control" value="{{$venta->direccion_ventas->cpp}}" readonly>
                        </div>
                    </div>
                    <h3 class="text-center">Costo</h3>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Tipo</th>
                            <th scope="col">Costo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Costo Regalo</td>
                                <td><input id="costoRegalo" type="text" class="form-control" value="${{$venta->productos->precio}}" readonly></td>
                            </tr>

                            <tr>
                                <td>Costo Envio</td>
                                <td><input id="costoEnvio" type="text" class="form-control" value="${{$venta->direccion_ventas->smza->costo_zonas->costo}}" readonly></td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td><input id="total" type="text" class="form-control" value="${{($venta->direccion_ventas->smza->costo_zonas->costo) + $venta->productos->precio}}" readonly></td>
                            </tr>
                        </tbody>
                    </table>                        
                        

                </div>
                
            </div>
        </div>
    
@endsection
