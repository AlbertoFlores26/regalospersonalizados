@extends('layouts.nav')

    @section('content')
        <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div> <!--Buscador-->

        <div class="container-fluid mt--7" style="">
            <div class="card bg-white shadow" style="padding-bottom: 20px">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Comprar Regalo</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('Tienda') }}" class="btn btn-sm btn-primary">Regresar</a>
                        </div>
                    </div>
                    <div class="col-12">
                    @if (\Session::has('request'))
                        <div class="alert alert-success">
                            <ul>
                                <li>{!! \Session::get('request') !!}</li>
                            </ul>
                        </div>
                    @endif
                </div>
                </div>
                <div class="container">
                    <form method="post" action="{{ route('Tienda.store' ) }}" autocomplete="off">
                    @csrf
                    <div class="row mt-5">
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">

                        <img src="{{ asset('imagen-producto/'. $producto->foto ) }}" class="card-img-top" alt="...">
                            <h4 class="text-center" style="color: #F89CA4; font-family: 'Rancho', cursive;">{{$producto->descripcionProducto}}</h4>
                            <h5 class="text-center" style="font-family: 'Rancho', cursive;">${{$producto->precio}}</h5>
                        </div>
                    </div>
                    <h3 class="text-center">Ingrese la dirección de entrega del regalo</h3>
                    <div class="row form-group">
                    <div class="col-12">
                        <label for="">Frase</label>
                            <input type="text"  name="frase_producto" class="form-control @error('frase_producto') is-invalid @enderror" maxlength="20" required>
                            <input type="hidden"  name="productos_id" class="form-control" value="{{$producto->id}}" required>
                            @error('frase_producto')
                                <div class="alert alert-danger">{{ $errors->first('frase_producto') }}</div>
                            @enderror
                        </div>
                    </div>
                        <div class="row form-group">
                            <div class="col-6">
                            <label for="">Mza</label>
                                <input type="text" id="mza" name="mza" class="form-control @error('mza') is-invalid @enderror" required>
                                @error('mza')
                                    <div class="alert alert-danger">{{ $errors->first('mza') }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="">lote</label>
                                <input type="text" id="lote" name="lote" class="form-control @error('lote') is-invalid @enderror" required>
                                @error('lote')
                                    <div class="alert alert-danger">{{ $errors->first('lote') }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-6">
                            <label for="">calle</label>
                                <input type="text" id="calle" name="calle" class="form-control @error('calle') is-invalid @enderror" required>
                                @error('calle')
                                    <div class="alert alert-danger">{{ $errors->first('calle') }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="">colonia</label>
                                <input type="text" id="colonia" name="colonia" class="form-control @error('colonia') is-invalid @enderror" required>
                                @error('colonia')
                                    <div class="alert alert-danger">{{ $errors->first('colonia') }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-6">
                            <label for="">noExterior</label>
                                <input type="text" id="noExterior" name="noExterior" class="form-control @error('noExterior') is-invalid @enderror" required>
                                @error('noExterior')
                                    <div class="alert alert-danger">{{ $errors->first('noExterior') }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="">noInterior</label>
                                <input type="text" id="noInterior" name="noInterior" class="form-control @error('noInterior') is-invalid @enderror" required>
                                @error('noInterior')
                                    <div class="alert alert-danger">{{ $errors->first('noInterior') }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-6">
                                <label for="">Smza</label>
                                <select id="smza_id" name="smza_id" id="" class="form-control @error('smza_id') is-invalid @enderror" required>
                                    @foreach ($smzas as $smza)
                                        <option value="{{$smza->id}}">{{$smza->smza}}</option>
                                    @endforeach
                                </select>
                                @error('smza_id')
                                    <div class="alert alert-danger">{{ $errors->first('smza_id') }}</div>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="">cpp</label>
                                <input type="text" id="cpp" name="cpp" class="form-control @error('cpp') is-invalid @enderror" required>
                                @error('cpp')
                                    <div class="alert alert-danger">{{ $errors->first('cpp') }}</div>
                                @enderror
                            </div>
                        </div>
                        <h3 class="text-center">Costo</h3>
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">Tipo</th>
                                <th scope="col">Costo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Costo Regalo</td>
                                    <td><input id="costoRegalo" type="text" class="form-control" value="${{$producto->precio}}" readonly></td>
                                </tr>

                                <tr>
                                    <td>Costo Envio</td>
                                    <td><input id="costoEnvio" type="text" class="form-control" readonly></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><input id="total" type="text" class="form-control" readonly></td>
                                </tr>
                            </tbody>
                        </table>                        
                        
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-6 center">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Guardar') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    
@endsection
