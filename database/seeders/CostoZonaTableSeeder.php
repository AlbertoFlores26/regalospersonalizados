<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CostoZona;
class CostoZonaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertCostoZona(5,'1',20.0);
        $this->insertCostoZona(5,'2',20.0);
        $this->insertCostoZona(5,'3',20.0);
        $this->insertCostoZona(5,'4',20.0);
        $this->insertCostoZona(5,'5',20.0);
        $this->insertCostoZona(5,'6',20.0);
        $this->insertCostoZona(5,'7',20.0);
        $this->insertCostoZona(5,'8',20.0);
        $this->insertCostoZona(5,'9',20.0);
        $this->insertCostoZona(5,'10',20.0);
        $this->insertCostoZona(5,'11',20.0);
        $this->insertCostoZona(5,'12',20.0);
        $this->insertCostoZona(5,'13',20.0);
        $this->insertCostoZona(5,'14',20.0);
        $this->insertCostoZona(5,'15',20.0);
        $this->insertCostoZona(5,'16',20.0);
        $this->insertCostoZona(5,'17',20.0);
        $this->insertCostoZona(5,'18',20.0);
        $this->insertCostoZona(5,'19',20.0);
        $this->insertCostoZona(5,'20',20.0);
        $this->insertCostoZona(5,'21',20.0);
        $this->insertCostoZona(5,'22',20.0);
        $this->insertCostoZona(5,'26',20.0);
        $this->insertCostoZona(5,'27',20.0);
        $this->insertCostoZona(5,'28',20.0);
        $this->insertCostoZona(5,'29',20.0);
        $this->insertCostoZona(5,'30',20.0);
        $this->insertCostoZona(5,'31',20.0);
        $this->insertCostoZona(5,'32',20.0);
        $this->insertCostoZona(5,'33',20.0);
        $this->insertCostoZona(5,'34',20.0);
        $this->insertCostoZona(5,'35',20.0);
        $this->insertCostoZona(5,'36',20.0);

        $this->insertCostoZona(5,'41',20.0);
        $this->insertCostoZona(5,'42',20.0);
        $this->insertCostoZona(5,'43',20.0);
        $this->insertCostoZona(5,'44',20.0);
        $this->insertCostoZona(5,'45',20.0);
        $this->insertCostoZona(5,'46',20.0);

        $this->insertCostoZona(5,'49',20.0);
        $this->insertCostoZona(5,'50',20.0);
        $this->insertCostoZona(5,'51',20.0);
        $this->insertCostoZona(5,'52',20.0);
        $this->insertCostoZona(5,'53',20.0);

    }
    private function insertCostoZona($municipio_id,$zona,$costo){
        $costoZona = new CostoZona();
        $costoZona->municipio_id = $municipio_id;
        $costoZona->zona = $zona;
        $costoZona->costo = $costo;
        $costoZona->save();
    }
}
